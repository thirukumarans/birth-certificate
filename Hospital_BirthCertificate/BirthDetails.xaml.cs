﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using Microsoft.Win32;
using System.IO;
using System.Windows.Markup;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Hospital_BirthCertificate
{
    /// <summary>
    /// Interaction logic for BirthDetails.xaml
    /// </summary>
    public partial class BirthDetails : Page
    {
     Dictionary<string, Array> rulesDictionary = new Dictionary<string, Array>();
     string filterKey = "mother_name";
        bool isStart = true;
        string birthId="";
        byte[] motherImage;
        byte[] fatherImage;
        public BirthDetails()
        {
            InitializeComponent();
            rulesDictionary.Add("txt_motherName", new[] { "NoInputValidation", "NameValidation" });
            rulesDictionary.Add("txt_fatherName", new[] { "NoInputValidation", "NameValidation" });
        }

        private void Page_Loaded_1(object sender, RoutedEventArgs e)
        {
            list.Height = Dock_Page.ActualHeight - 78;
            if (isStart)
            {
                var url = (NavigationService.CurrentSource).OriginalString;
                var values = GetParams(url);
                var pageData = values.ContainsKey("pageData") ? values["pageData"] : null;
                var loadType = values.ContainsKey("loadType") ? values["loadType"] : null;

                if (pageData != null)
                {
                    LoadBirthData(pageData);
                }
                else if (loadType == "Filter")
                {
                    btn_save.Content = "Search";
                }
                creatList("");
                isStart = false;
            }
        }

        private void creatList(string key)
        {
            DBManager data = new DBManager();
            var tabel = data.GetBirthList("null");

            if (dropButton.ContextMenu.Items.Count == 0)
            {
                dropButton.ContextMenu.Items.Clear();
                foreach (var t in tabel.Columns)
                {
                    MenuItem menu = new MenuItem();
                    menu.Click += new RoutedEventHandler(account_FilterOptionChangeMenu);
                    menu.Header = Convert.ToString(t);
                    if (Convert.ToString(t) == filterKey)
                    {
                        menu.IsChecked = true;
                    }
                    dropButton.ContextMenu.Items.Add(menu);
                }
            }
            int i = 0;
            var x = tabel.AsEnumerable().Where((r) => ((Convert.ToString(r[filterKey])).ToLower().Contains(key))).Select(r => new
            {
                name = (string)r["mother_name"],
                birthId = Convert.ToString(r["birth_id"]),
                id = Convert.ToString(r["father_name"]),
                price = Convert.ToString(r["baby_gender"]),
                group = Convert.ToString(r[filterKey])
            });
            count.Content="Count : "+x.Count();
            foreach (var t in x)
            {
                ListBoxItem item = new ListBoxItem();
                item.Tag = t.birthId;
                item.Background = new SolidColorBrush((Color)FindResource("Gray8"));
                if (i != 0)
                {
                    item.Margin = new Thickness(0, 4, 0, 0);
                }
                else
                {
                    i++;
                }
                TextBlock te = new TextBlock();
                te.Text = t.name;
                te.Foreground = new SolidColorBrush((Color)FindResource("AccentColor"));
                te.FontSize = 20;
                TextBlock te1 = new TextBlock();
                te1.Text = "Father: " + t.id;
                te1.Foreground = new SolidColorBrush((Color)FindResource("BlackColor"));
                te1.FontSize = 16;
                TextBlock te2 = new TextBlock();
                te2.Text = "Baby: " + t.price;
                te2.Foreground = new SolidColorBrush((Color)FindResource("BlackColor"));
                te2.FontSize = 16;
                TextBlock te3 = new TextBlock();
                te3.Text = filterKey + ": " + t.group;
                te3.Foreground = new SolidColorBrush((Color)FindResource("BlackColor"));
                te3.FontSize = 16;
                StackPanel st = new StackPanel();
                st.Children.Add(te);
                st.Children.Add(te1);
                st.Children.Add(te2);
                st.Children.Add(te3);
                item.Content = st;
                list.Items.Add(item);
            }
        }

        private void account_FilterOptionChange(object sender, RoutedEventArgs e)
        {
            dropButton.ContextMenu.IsOpen = false;
            list.Items.Clear();
            filterKey = ((Control)sender).Name;
            creatList("");
        }


        private void account_FilterOptionChangeMenu(object sender, RoutedEventArgs e)
        {
            list.Items.Clear();
            MenuItem obj = (MenuItem)sender;
            var items = dropButton.ContextMenu.Items;
            foreach (var item in items)
            {
                ((MenuItem)item).IsChecked = false;
            }
            filterKey = (string)obj.Header;
            creatList("");
            obj.IsChecked = true;
        }

        //private void radio_FilterOptionChange(object sender, RoutedEventArgs e)
        //{
        //    list.Items.Clear();
        //    filterKey = ((Control)sender).Name;
        //    creatList("");
        //}

        private void filterClick(object sender, RoutedEventArgs e)
        {
            dropButton.ContextMenu.IsEnabled = true;
            dropButton.ContextMenu.PlacementTarget = (e.OriginalSource as UIElement);
            dropButton.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            dropButton.ContextMenu.IsOpen = true;
        }

        private void filter_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            list.Items.Clear();
            creatList(filter.Text.ToLower());
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            birthId = "";
            var url = (NavigationService.CurrentSource).OriginalString;
            if (url != "BirthDetails.xaml")
            {
                this.NavigationService.Navigate(new System.Uri("BirthDetails.xaml", UriKind.Relative));
            }
            else
            {
                this.NavigationService.Refresh();
            }
        }

        private void control_TestChanged(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                string name = ((TextBox)sender).Name;
                validate(name, rulesDictionary[name]);
            }

        }

        private bool validate(string name, Array rules)
        {
            bool isValid = true;
            var val = new ValueValidation();
            NoInputValidation mandatary = new NoInputValidation();
            TextBox ctrl = (TextBox)this.FindName(name);
            string ErrorMsg = val.Validate(ctrl.Text, rules);
            if (ErrorMsg != null)
            {
                ValidationError validationError = new ValidationError(mandatary, ctrl.GetBindingExpression(TextBox.TextProperty));
                validationError.ErrorContent = ErrorMsg;
                Validation.MarkInvalid(ctrl.GetBindingExpression(TextBox.TextProperty), validationError);
                isValid = false;
            }
            return isValid;
        }

        private void EnterKeyNavigation(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Control feSource = e.Source as Control;
                if (feSource.Name == "txt_taxName")
                {
                    //cmb_taxType.IsDropDownOpen = true;
                }
                feSource.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private bool validateItemGroup(string type)
        {
            bool isValid = true;
            DBManager data = new DBManager();
            NoInputValidation mandatary = new NoInputValidation();
            foreach (KeyValuePair<string, Array> item in rulesDictionary)
            {
                if (!validate(item.Key, item.Value))
                {
                    isValid = false;
                }
            }
            return isValid;
        }

        private void btn_save_Click_1(object sender, RoutedEventArgs e)
        {
            DBManager data = new DBManager();
            if ((string)btn_save.Content == "Search")
            {
                var tempKey = "";// txt_taxName.Text + "," + cmb_taxType.Text + "," + txt_Rate + "," + cmb_regional + "," + txt_desc.Text;
                this.NavigationService.Navigate(new System.Uri("/DataView.xaml?page=Birth&pageData=" + tempKey, UriKind.Relative));
            }
            else if ((string)btn_save.Content == "Update")
            {
                if (validateItemGroup("update"))
                {
                    data.UpdateBirthDetails(birthId, txt_MotherId.Text, txt_BabyId.Text, txt_motherName.Text, txt_fatherName.Text, motherImage, fatherImage, cmb_baby.Text, txt_Date.Text, Convert.ToString(txt_Hour.Value), Convert.ToString(txt_Minute.Value), Cmb_AMrPM.Text, txt_streetAddress.Text, txt_city.Text, txt_state.Text, txt_country.Text, txt_pincode.Text, txt_desc.Text);
                    btn_cancel_Click(new object(), new RoutedEventArgs());
                }
            }
            else
            {
                if (validateItemGroup("add"))
                {
                    data.InsertBirth(txt_motherName.Text, txt_MotherId.Text, txt_BabyId.Text, txt_fatherName.Text, motherImage, fatherImage, cmb_baby.Text, txt_Date.Text, Convert.ToString(txt_Hour.Value), Convert.ToString(txt_Minute.Value), Cmb_AMrPM.Text, txt_streetAddress.Text, txt_city.Text, txt_state.Text, txt_country.Text, txt_pincode.Text, txt_desc.Text);
                    btn_cancel_Click(new object(), new RoutedEventArgs());
                }
            }
        }

        private void list_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            var st = (ListBoxItem)list.SelectedItem;
            if (st != null)
            {
                LoadBirthData((string)st.Tag);
            }
        }

        private void LoadBirthData(string pageData)
        {
            btn_print.Visibility = Visibility.Visible;
            btn_save.Content = "Update";
            btn_Delete.Visibility = Visibility.Visible;
            if (DBManager.isAdmin == false)
            {
                btn_save.Visibility = Visibility.Hidden;
                btn_Delete.Visibility = Visibility.Hidden;
            }
            DBManager data = new DBManager();
            List<object> details = data.GetBirthDetails(pageData);
            lbl_Header.Content="Birth Certificate Id-" +  Convert.ToString(details[0]);
            birthId =  Convert.ToString(details[0]);
            txt_MotherId.Text= Convert.ToString(details[1]);
            txt_BabyId.Text = Convert.ToString(details[2]);
            txt_motherName.Text= Convert.ToString(details[3]);
            txt_fatherName.Text= Convert.ToString(details[4]);
            //motherImage= Convert.ToString(details[3]);
            if (details[5].GetType() == typeof(byte[]))
            {
                motherImage = (byte[])details[5];
                byte[] imgByte = (byte[])details[5];

                MemoryStream strm = new MemoryStream();

                strm.Write(imgByte, 0, imgByte.Length);

                strm.Position = 0;

                System.Drawing.Image img = System.Drawing.Image.FromStream(strm);

                BitmapImage bi = new BitmapImage();

                bi.BeginInit();

                MemoryStream ms = new MemoryStream();

                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

                ms.Seek(0, SeekOrigin.Begin);

                bi.StreamSource = ms;

                bi.EndInit();

                Img_mother.Source = bi;
            }
            else
            {
                motherImage = null;
                Img_mother.Source = new BitmapImage(new Uri(@"\Image.png", UriKind.Relative)); ;
            }

            if (details[6].GetType() == typeof(byte[]))
            {
                fatherImage = (byte[])details[6];
                byte[] imgByte = (byte[])details[6];

                MemoryStream strm = new MemoryStream();

                strm.Write(imgByte, 0, imgByte.Length);

                strm.Position = 0;

                System.Drawing.Image img = System.Drawing.Image.FromStream(strm);

                BitmapImage bi = new BitmapImage();

                bi.BeginInit();

                MemoryStream ms = new MemoryStream();

                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

                ms.Seek(0, SeekOrigin.Begin);

                bi.StreamSource = ms;

                bi.EndInit();

                Img_father.Source = bi;
            }
            else
            {
                fatherImage = null;
                Img_father.Source = new BitmapImage(new Uri(@"\Men.png", UriKind.Relative)); ;
            }
            cmb_baby.Text= Convert.ToString(details[7]);
            txt_Date.Text= Convert.ToString(details[8]);
            txt_Hour.Value= Convert.ToDouble(details[9]);
            txt_Minute.Value = Convert.ToDouble(details[10]);
            Cmb_AMrPM.Text = Convert.ToString(details[11]);
            txt_streetAddress.Text = Convert.ToString(details[12]);
            txt_city.Text = Convert.ToString(details[13]);
            txt_state.Text = Convert.ToString(details[14]);
            txt_country.Text = Convert.ToString(details[15]);
            txt_pincode.Text = Convert.ToString(details[16]);
            txt_desc.Text= Convert.ToString(details[17]);
        }

        private void btn_Delete_Click_1(object sender, RoutedEventArgs e)
        {
            DBManager data = new DBManager();
            data.DeleteMasterRecord("contacts", "birth_id", birthId);
            btn_cancel_Click(new object(), new RoutedEventArgs());
        }

        private void ViewAll_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new System.Uri("DataView.xaml?page=Birth&pageData=null", UriKind.Relative));
        }

        private void Viewall_Filter_Click(object sender, RoutedEventArgs e)
        {
            btn_save.Content = "Search";
        }

        private void page_Unloaded(object sender, RoutedEventArgs e)
        {
            list.Height = 300;
        }

        public static Dictionary<string, string> GetParams(string uri)
        {
            var matches = Regex.Matches(uri, @"[\?&](([^&=]+)=([^&=#]*))", RegexOptions.Compiled);
            return matches.Cast<Match>().ToDictionary(
                m => Uri.UnescapeDataString(m.Groups[2].Value),
                m => Uri.UnescapeDataString(m.Groups[3].Value)
            );
        }

        private void motherImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp;*.gif|" +
                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "GIF File (*.gif)|*.gif|" +
                "Bitmap image (*.bmp)|*.bmp|" +
                "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                FileStream fs = new FileStream(op.FileName, FileMode.Open, FileAccess.Read);
                Img_mother.Source = new BitmapImage(new Uri(op.FileName));
                motherImage = new byte[fs.Length];
                fs.Read(motherImage, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
            }
        }

        private void fatherImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp;*.gif|" +
                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "GIF File (*.gif)|*.gif|" +
                "Bitmap image (*.bmp)|*.bmp|" +
                "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                FileStream fs = new FileStream(op.FileName, FileMode.Open, FileAccess.Read);
                Img_father.Source = new BitmapImage(new Uri(op.FileName));
                fatherImage = new byte[fs.Length];
                fs.Read(fatherImage, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
            }
        }

        private void Print_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (birthId != "")
                {
                    PrintDialog printDlg = new PrintDialog();
                    var date = txt_Date.DisplayDate.ToLongDateString();
                    FileStream file = new FileStream(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)+"\\BirthCertificateTemplate.xaml", FileMode.Open, FileAccess.Read);
                    FlowDocument oDoc = XamlReader.Load(file) as FlowDocument;
                    var time = (Convert.ToDateTime(txt_Hour.Value + ":" + txt_Minute.Value + Cmb_AMrPM.Text)).ToShortTimeString();
                    (oDoc.FindName("birthId") as Run).Text = birthId;
                    if (txt_BabyId.Text != "")
                    {
                        (oDoc.FindName("BabyNo") as Run).Text = txt_BabyId.Text;
                    }
                    if (txt_MotherId.Text != "")
                    {
                        (oDoc.FindName("MotherNo") as Run).Text = txt_MotherId.Text;
                    }
                    (oDoc.FindName("babyGender") as Run).Text = cmb_baby.Text;
                    (oDoc.FindName("mother") as Run).Text = txt_motherName.Text;
                    (oDoc.FindName("father") as Run).Text = txt_fatherName.Text + ", " + txt_streetAddress.Text + ", " + txt_city.Text + ", " + txt_state.Text + ", " + txt_country.Text + " -" + txt_pincode.Text;
                    (oDoc.FindName("time") as Run).Text = time;
                    (oDoc.FindName("Date") as Run).Text = date;
                    var x = Img_mother.Source;
                    if (motherImage != null)
                    {
                        (oDoc.FindName("mother_Img") as Image).Source = Img_mother.Source;
                    }
                    if (fatherImage != null)
                    {
                        (oDoc.FindName("father_Img") as Image).Source = Img_father.Source;
                    }
                    if (fatherImage == null && motherImage == null)
                    {
                        (oDoc.FindName("imageDesc") as Run).Text = " ";
                    }
                    IDocumentPaginatorSource idpSource = oDoc;
                    PrintDialog printDialog = new PrintDialog();
                    oDoc.PageHeight = printDialog.PrintableAreaHeight;
                    oDoc.PageWidth = printDialog.PrintableAreaWidth;
                    oDoc.PagePadding = new Thickness(25);

                    oDoc.ColumnGap = 0;

                    oDoc.ColumnWidth = (oDoc.PageWidth -
                                           oDoc.ColumnGap -
                                           oDoc.PagePadding.Left -
                                           oDoc.PagePadding.Right);
                    printDlg.PrintDocument(idpSource.DocumentPaginator, "Contact Manager.");
                    MessageBox.Show("Print Status....", "Print done successfully");
                    //ShowMessageDialog("Print Status....", "Print done successfully");
                }
                else
                {

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Print Status....", ex.Message);
            }

        }

        //private async void ShowMessageDialog(string heading, string message)
        //{
        //    var x1 = (MetroWindow)Window.GetWindow(this);
        //    await x1.ShowMessageAsync(heading, message);
        //}

        //private async Task PrintBirthCertificate()
        //{
        //    if (birthId != "")
        //    {
        //        PrintDialog printDlg = new PrintDialog();
        //        var date = txt_Date.DisplayDate.ToLongDateString();
        //        FileStream file = new FileStream("BirthCertificateTemplate.xaml", FileMode.Open, FileAccess.Read);
        //        FlowDocument oDoc = XamlReader.Load(file) as FlowDocument;
        //        var time = (Convert.ToDateTime(txt_Hour.Value + ":" + txt_Minute.Value + Cmb_AMrPM.Text)).ToShortTimeString();
        //        (oDoc.FindName("birthId") as Run).Text = birthId;
        //        if (txt_BabyId.Text != "")
        //        {
        //            (oDoc.FindName("BabyNo") as Run).Text = txt_BabyId.Text;
        //        }
        //        if (txt_MotherId.Text != "")
        //        {
        //            (oDoc.FindName("MotherNo") as Run).Text = txt_MotherId.Text;
        //        }
        //        (oDoc.FindName("babyGender") as Run).Text = cmb_baby.Text;
        //        (oDoc.FindName("mother") as Run).Text = txt_motherName.Text;
        //        (oDoc.FindName("father") as Run).Text = txt_fatherName.Text + ", " + txt_streetAddress.Text + ", " + txt_city.Text + ", " + txt_state.Text + ", " + txt_country.Text + " -" + txt_pincode.Text;
        //        (oDoc.FindName("time") as Run).Text = time;
        //        (oDoc.FindName("Date") as Run).Text = date;
        //        var x = Img_mother.Source;
        //        if (motherImage != null)
        //        {
        //            (oDoc.FindName("mother_Img") as Image).Source = Img_mother.Source;
        //        }
        //        if (fatherImage != null)
        //        {
        //            (oDoc.FindName("father_Img") as Image).Source = Img_father.Source;
        //        }
        //        if (fatherImage == null && motherImage == null)
        //        {
        //            (oDoc.FindName("imageDesc") as Run).Text = " ";
        //        }
        //        IDocumentPaginatorSource idpSource = oDoc;
        //        PrintDialog printDialog = new PrintDialog();
        //        oDoc.PageHeight = printDialog.PrintableAreaHeight;
        //        oDoc.PageWidth = printDialog.PrintableAreaWidth;
        //        oDoc.PagePadding = new Thickness(25);

        //        oDoc.ColumnGap = 0;

        //        oDoc.ColumnWidth = (oDoc.PageWidth -
        //                               oDoc.ColumnGap -
        //                               oDoc.PagePadding.Left -
        //                               oDoc.PagePadding.Right);
        //        printDlg.PrintDocument(idpSource.DocumentPaginator, "Contact Manager.");
        //        ShowMessageDialog("Requested birth certificate print completed successfully.. :)", "Print Status....");
        //    }
        //    else
        //    {

        //    }
        //}

        private void ShowPrintDialog(object sender, RoutedEventArgs e)
        {
            try{
            LoadBirthData(birthId);
            System.OperatingSystem os = Environment.OSVersion;
            Version vs = os.Version;
            //if (vs.Major != 5)
            //{
            //    ShowAsyncDialog();
            //}
            //else
            //{
                Print_Click("", new RoutedEventArgs());
            //}
        }
            catch(Exception ex)
            {
                MessageBox.Show("Print Status....", ex.Message);
            }
        }

        //private async void ShowAsyncDialog()
        //{
        //    var x1 = (MetroWindow)Window.GetWindow(this);
        //    var controller = await x1.ShowProgressAsync("Please wait...", "please wait for print to complete successfully.. !");

        //    await PrintBirthCertificate();
        //    await controller.CloseAsync();
        //    await x1.ShowMessageAsync("Print completed..", "Requested birth certificate print completed successfully.. :)");
        //}

        //private async void ShowProgressDialog(object sender, RoutedEventArgs e)
        //{
        //    var x1 = (MetroWindow)Window.GetWindow(this);
        //    //this.MetroDialogOptions.ColorScheme = UseAccentForDialogsMenuItem.IsChecked ? MetroDialogColorScheme.Accented : MetroDialogColorScheme.Theme;
        //    var result = await x1.ShowInputAsync("test", "test", null);
        //    if (result == null)
        //    {
        //        //User pressed cancel
        //    }
        //    else
        //    {
        //        //MessageDialogResult messageResult = await x1.ShowMessageAsync("Authentication Information", String.Format("Username: {0}\nPassword: {1}", result.Username, result.Password));
        //    }
        //}
    }
}


