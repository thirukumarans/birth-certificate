﻿using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hospital_BirthCertificate
{
    /// <summary>
    /// Interaction logic for LoginData.xaml
    /// </summary>
    public partial class LoginData : Page
    {
        public LoginData()
        {
            InitializeComponent();
            string dir = AppDomain.CurrentDomain.BaseDirectory + "DataBase-Sqlite\\";            
            txt_UserName.Focus();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if (validateItem())
            {
                DBManager data = new DBManager();
                string value = data.VerifyUser(txt_UserName.Text, txt_password.Password);
                if (value == null)
                {
                    NoInputValidation mandatary = new NoInputValidation();
                    ValidationError validationError = new ValidationError(mandatary, txt_UserName.GetBindingExpression(TextBox.TextProperty));
                    validationError.ErrorContent = "Invalid Username Or Password";
                    Validation.MarkInvalid(txt_UserName.GetBindingExpression(TextBox.TextProperty), validationError);
                    txt_UserName.Focus();
                    return;
                }
                else if (value == "user")
                {
                    this.NavigationService.Navigate(new System.Uri("BirthDetails.xaml", UriKind.Relative));
                    DBManager.isAdmin = true;
                }
            }
        }

        private bool validateItem()
        {
            bool validation = true;
            NoInputValidation mandatary = new NoInputValidation();
            if (txt_UserName.Text == "" || txt_UserName.Text == null)
            {
                ValidationError validationError = new ValidationError(mandatary, txt_UserName.GetBindingExpression(TextBox.TextProperty));
                validationError.ErrorContent = "Can't Be Empty";
                Validation.MarkInvalid(txt_UserName.GetBindingExpression(TextBox.TextProperty), validationError);
                validation = false;
                txt_UserName.Focus();
            }
            return validation;
        }

        private void EnterKeyNavigation(object sender, KeyEventArgs e)
        {
            Control feSource = e.Source as Control;
            if (e.Key == Key.Enter)
            {
                feSource.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private void ChangePassword(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new System.Uri("ChangePassword.xaml", UriKind.Relative));
        }
    }
}
