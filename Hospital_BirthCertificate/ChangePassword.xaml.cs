﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Hospital_BirthCertificate
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Page
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            DBManager data = new DBManager();
            if (txt_NewPassword.Password == txt_ConfirmPassword.Password)
            {
                if (data.VerifyUserandChange(txt_OldPassword.Password, txt_NewPassword.Password))
                {
                    ShowMessage("Password changed....!", "Given password changed sucessfully..........!");
                    DBManager.isAdmin = false;
                    this.NavigationService.Navigate(new System.Uri("LoginData.xaml", UriKind.Relative));
                }
                else
                {
                    ShowMessage("Invalid Data....!", "Given password is invalid..........!");
                }
            }
            else
            {
                ShowMessage("Invalid Data....!", "New password dose not match..........!");
            }
        }

        private bool validateItem()
        {
            bool validation = true;
            DBManager data=new DBManager();
            if (txt_NewPassword.Password == txt_ConfirmPassword.Password)
            {
                if (data.VerifyUserandChange(txt_OldPassword.Password,txt_NewPassword.Password))
                {
                    ShowMessage("Invalid Data....!", "Given password is invalid..........!");
                }
                else
                {
                    ShowMessage("Invalid Data....!","Given password is invalid..........!");
                }
            }
            else
            {
                ShowMessage("Invalid Data....!", "New password dose not match..........!");
            }
            return validation;
        }

        private void EnterKeyNavigation(object sender, KeyEventArgs e)
        {
            Control feSource = e.Source as Control;
            if (e.Key == Key.Enter)
            {
                feSource.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private void LoginPage(object sender, RoutedEventArgs e)
        {
            if (DBManager.isAdmin == false)
            {
                this.NavigationService.Navigate(new System.Uri("LoginData.xaml", UriKind.Relative));
            }
            else
            {
                DBManager.isAdmin = false;
                this.NavigationService.Navigate(new System.Uri("BirthDetails.xaml", UriKind.Relative));
            }

        }
        private void ShowMessage(string header, string message)
        {
            System.OperatingSystem os = Environment.OSVersion;
            Version vs = os.Version;
            if (vs.Major != 5)
            {
                ShowAsycMessage(header, message);
            }
            else
            {
                MessageBox.Show(message,header);
            }
        }

        private async void ShowAsycMessage(string header, string message)
        {
            var x1 = (MetroWindow)Window.GetWindow(this);
            await x1.ShowMessageAsync(header, message);
        }
    }
}
