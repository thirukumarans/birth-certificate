﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Hospital_BirthCertificate
{
    class ValueValidation
    {
        private string Value { get; set; }
        private string ErrorMsg { get; set; }
        public string Validate(string value,Array rules)
        {
            Value = value;
            ErrorMsg = null;
            foreach (string rule in rules)
            {
                if (rule.StartsWith("InGroup"))
                {
                    InGroup(rule);
                }
                else
                {
                    Type thisType = this.GetType();
                    MethodInfo theMethod = thisType.GetMethod(rule);
                    if (!Convert.ToBoolean(theMethod.Invoke(this, null)))
                    {
                        break;
                    }
                }
            }
            return ErrorMsg;
        }

        public bool IntegerValidation()
        {
            uint parameter = 0;

            try
            {
                if ((Value).Length > 0)
                {
                    parameter = UInt32.Parse(Value);
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Only integers allowed";
                return false;
            }
            return true;
        }

        public bool FloatValidation()
        {
            float parameter = 0;

            try
            {
                if ((Value).Length > 0)
                {
                    parameter = float.Parse(Value);
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Only integers allowed";
                return false;
            }
            return true;
        }

        public bool link()
        {
            return true;
        }

        public bool NoInputValidation()
        {
            try
            {
                if (Value!="" || Value==null)
                {
                    return true;
                }
                else
                {
                    ErrorMsg = "Can't be empty";
                    return false;
                }
            }
            catch (Exception)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool ItemGroupValidation()
        {
          return true;
        }

        public bool TaxGroupValidation()
        {
          return true;
        }

        public bool InGroup(string group)
        {
            if ((Value).Length <= 0)
            {
                return true;
            }
            var val = group.Split('+');
            var groupArr = val[1].Split(',');
            if(groupArr.Contains(Value))
            {
                return true;
            }
            else
            {
                ErrorMsg = "Invalid value";
                return false;
            }
        }

        public bool NameValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, "^(?!^(PRN|AUX|CLOCK\\$|NUL|CON|COM\\d|LPT\\d|\\..*)(\\..+)?$)[^\x00-\x1f\\?*:\";|/]+$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid characters \\ / : * ? \" < > | used";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool FileNameValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, "^(?!^(PRN|AUX|CLOCK\\$|NUL|CON|COM\\d|LPT\\d|\\..*)(\\..+)?$)[^\x00-\x1f\\?*:\";|/]+$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid characters \\ / : * ? \" < > | used";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool RupeeValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^\d+(?:\.\d{0,2})?$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid input Ex:23.22,2332 ";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool PhoneNumberValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Phone number";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool EmailValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Email Address ";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool FaxValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^(((((((00|\+)49[ \-/]?)|0)[1-9][0-9]{1,4})[ \-/]?)|((((00|\+)49\()|\(0)[1-9][0-9]{1,4}\)[ \-/]?))[0-9]{1,7}([ \-/]?[0-9]{1,5})?)$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Fax";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool PincodeValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"([0-9]{6}|[0-9]{3}\s[0-9]{3})"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Pincode";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool NumberValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^([0-9]*|\d*\.\d{1}?\d*)$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Number";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool VariableValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^[a-zA-Z][a-zA-Z0-9_]*$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Input";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }

        public bool AccountNoValidation()
        {
            try
            {
                if (Regex.IsMatch(Value, @"^[a-zA-Z0-9][a-zA-Z0-9_]*$"))
                {
                    return true;
                }
                else if (Value != "" || Value == null)
                {
                    ErrorMsg = "Invalid Input";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                ErrorMsg = "Invalid characters ";
                return false;
            }
        }
    }
}
