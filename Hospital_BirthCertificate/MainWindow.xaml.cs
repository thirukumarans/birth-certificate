﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hospital_BirthCertificate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            CultureInfo ci = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd-MM-yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
        }

        private void frame_LoadCompleted(object sender, NavigationEventArgs e)
        {
            if (DBManager.isAdmin == true)
            {
                Btn_logout.Visibility = Visibility.Visible;
            }
            else
            {
                Btn_logout.Visibility = Visibility.Hidden;
            }
            animation.Reload();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var x = Convert.ToString(frame_Home.Source);
            if (x == "BirthDetails.xaml")
            {
                if (DBManager.isAdmin == false)
                {
                    frame_Home.Navigate(new System.Uri("/LoginData.xaml", UriKind.Relative));
                }
                else
                {
                    frame_Home.Navigate(new System.Uri("/ChangePassword.xaml", UriKind.Relative));
                }
            }
            else
            {
                frame_Home.Navigate(new System.Uri("/BirthDetails.xaml", UriKind.Relative));
            }
        }

        private void Btn_logout_Click_1(object sender, RoutedEventArgs e)
        {
            DBManager.isAdmin = false;
            frame_Home.Navigate(new System.Uri("/BirthDetails.xaml", UriKind.Relative));
            frame_Home.Refresh();
        }
    }
}
