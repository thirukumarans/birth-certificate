﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SQLite;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Hospital_BirthCertificate
{
    public class DBManager
    {
        
        //MSSQL
        public static String connectionString = @"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "\\DataBase-Sqlite\\BirthCertificate.sqlite;Version=3;";
        SQLiteConnection conn = new SQLiteConnection(connectionString);
        public static bool isAdmin = false;
        //Function for getting Item Group List
        public DataTable GetBirthList(string keys)
        {
            try
            {
                conn.Open();
                string[] filters = keys.Split(',');
                if (keys == "null" || filters.Length == 1)
                {
                    SQLiteCommand myCommand = new SQLiteCommand("select * from contacts;", conn);
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(myCommand);
                    DataTable dt = new DataTable("AccountGroup");
                    sda.Fill(dt);
                    return dt;
                }
                else
                {
                    SQLiteCommand myCommand = new SQLiteCommand("select * from contacts where name like @filter0 and  type like @filter1 and  rate like @filter2 and  regional like @filter3 and  comment like @filter4;", conn);
                    for (int i = 0; i < 5; i++)
                    {
                        myCommand.Parameters.AddWithValue("@filter" + i, "%" + filters[i] + "%");
                    }
                    SQLiteDataAdapter sda = new SQLiteDataAdapter(myCommand);
                    DataTable dt = new DataTable("contacts");
                    sda.Fill(dt);
                    return dt;
                }
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public bool UpdateBirthDetails(string birth_id,string motherNo,string babyNo, string motherName, string fatherName, byte[] motherImage, byte[] fatherImage, string babyGender, string date, string hour, string minute, string AMrPM, string streetAddress, string city, string state, string country, string pincode, string comment)
        {
            try
            {
                //string code1 = code.Text;
                conn.Open();
                //insert into ab_account values ('test','Capital','4523685',456,523,6000,'none');mother_no, baby_no
                //birth_id,mother_name,father_name,mother_image,father_image,baby_gender,date,hour,minute,Description
                SQLiteCommand myCommand = new SQLiteCommand("UPDATE contacts SET mother_name = @motherName,mother_no=@motherNo,baby_no=@babyNo,father_name = @fatherName,mother_image = @motherImage,father_image = @fatherImage,baby_gender = @babyGender,date = @date,hour = @hour,minute = @minute,AMrPM=@AMrPM,street_address=@streetAddress,city = @city,state=@state,country = @country,pincode=@pincode,Description = @comment WHERE birth_id=@birth_id;", conn);
                myCommand.Parameters.AddWithValue("@motherName", motherName);
                myCommand.Parameters.AddWithValue("@motherNo", motherNo);
                myCommand.Parameters.AddWithValue("@babyNo", babyNo);
                myCommand.Parameters.AddWithValue("@fatherName", fatherName);
                myCommand.Parameters.AddWithValue("@motherImage", motherImage);
                myCommand.Parameters.AddWithValue("@fatherImage", fatherImage);
                myCommand.Parameters.AddWithValue("@babyGender", babyGender);
                myCommand.Parameters.AddWithValue("@date", date);
                myCommand.Parameters.AddWithValue("@hour", hour);
                myCommand.Parameters.AddWithValue("@minute", minute);
                myCommand.Parameters.AddWithValue("@AMrPM", AMrPM);
                myCommand.Parameters.AddWithValue("@streetAddress", streetAddress);
                myCommand.Parameters.AddWithValue("@city", city);
                myCommand.Parameters.AddWithValue("@state", state);
                myCommand.Parameters.AddWithValue("@country", country);
                myCommand.Parameters.AddWithValue("@pincode", pincode);
                myCommand.Parameters.AddWithValue("@comment", comment);
                myCommand.Parameters.AddWithValue("@birth_id", birth_id);
                myCommand.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public bool InsertBirth(string motherName, string motherNo, string babyNo, string fatherName, byte[] motherImage, byte[] fatherImage, string babyGender, string date, string hour, string minute, string AMrPM, string streetAddress, string city, string state, string country, string pincode, string comment)
        {
            try
            {
                //string code1 = code.Text;
                conn.Open();
                //insert into ab_account values ('test','Capital','4523685',456,523,6000,'none');
                SQLiteCommand myCommand = new SQLiteCommand("insert into contacts(mother_name,mother_no, baby_no,father_name,mother_image,father_image,baby_gender,date,hour,minute,AMrPM,street_address,city,state,country,pincode,Description) values(@motherName,@motherNo, @babyNo, @fatherName, @motherImage,@fatherImage,@babyGender,@date,@hour,@minute,@AMrPM,@streetAddress,@city,@state,@country,@pincode,@comment);", conn);
                myCommand.Parameters.AddWithValue("@motherName", motherName);
                myCommand.Parameters.AddWithValue("@motherNo", motherNo);
                myCommand.Parameters.AddWithValue("@babyNo", babyNo);
                myCommand.Parameters.AddWithValue("@fatherName", fatherName);
                myCommand.Parameters.AddWithValue("@motherImage", motherImage);
                myCommand.Parameters.AddWithValue("@fatherImage", fatherImage);
                myCommand.Parameters.AddWithValue("@babyGender", babyGender);
                myCommand.Parameters.AddWithValue("@date", date);
                myCommand.Parameters.AddWithValue("@hour", hour);
                myCommand.Parameters.AddWithValue("@minute", minute);
                myCommand.Parameters.AddWithValue("@AMrPM", AMrPM);
                myCommand.Parameters.AddWithValue("@streetAddress", streetAddress);
                myCommand.Parameters.AddWithValue("@city", city);
                myCommand.Parameters.AddWithValue("@state", state);
                myCommand.Parameters.AddWithValue("@country", country);
                myCommand.Parameters.AddWithValue("@pincode", pincode);
                myCommand.Parameters.AddWithValue("@comment", comment);
                myCommand.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public List<object> GetBirthDetails(string no)
        {
            List<object> data = new List<object>();
            try
            {
                conn.Open();
                SQLiteCommand myCommand = new SQLiteCommand("select * from contacts where birth_id=@no;", conn);
                myCommand.Parameters.AddWithValue("@no", no);
                SQLiteDataReader reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        data.Add(reader.GetValue(i));
                    }
                }
                return data;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public bool DeleteMasterRecord(string masterTabelName, string coloum, string value)
        {
            try
            {
                conn.Open();
                SQLiteCommand myCommand = new SQLiteCommand("DELETE FROM " + masterTabelName + " WHERE " + coloum + "=@Value;", conn);
                myCommand.Parameters.AddWithValue("@Value", value);
                myCommand.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public string VerifyUser(string userName,string password)
        {
            if (Properties.Settings.Default.UserName == userName && Properties.Settings.Default.User == password)
            {
                    return "user";
            }
            return null;
        }

        public bool VerifyUserandChange(string password,string newPassword)
        {
            if (Properties.Settings.Default.User == password)
            {
                Properties.Settings.Default.User = newPassword;
                Properties.Settings.Default.Save();
                return true;
            }
            return false;
        }
    }
}
