﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Hospital_BirthCertificate
{
    class MinMaxValidation : ValidationRule
    {
        public double Min { get; set; }

        public double Max { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            this.Min = 0;
            this.Max = 10;
            double parameter = 0;

            try
            {
                if (((string)value).Length > 0)
                {
                    parameter = Int64.Parse((String)value);
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Illegal characters or " + e.Message);
            }

            if ((parameter < this.Min) || (parameter > this.Max))
            {
                return new ValidationResult(false, "Please enter value in the range: " + this.Min + " - " + this.Max + ".");
            }
            return new ValidationResult(true, null);
        }
    }

    class NoInputValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (((string)value) == "" || ((string)value) == null)
                {
                    return new ValidationResult(false, "Enter value");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Only integers allowed or " + e.Message);
            }
            return new ValidationResult(true, null);
        }
    }

    class IntegerValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            uint parameter = 0;

            try
            {
                if (((string)value).Length > 0)
                {
                    parameter = UInt32.Parse((String)value);
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Only integers allowed or " + e.Message);
            }
            return new ValidationResult(true, null);
        }
    }

    class NameValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^([a-zA-Z]+(?:\.)?(?: [a-zA-Z]+(?:\.)?)*)$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid characters ");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }
            
        }
    }
    //^\d+(?:\.\d{0,2})?$   -amount

    class RupeeValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^\d+(?:\.\d{0,2})?$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid input Ex:23.22,2332 ");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }

    class PhoneNumberValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid Phone number ");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }

    class EmailValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid Email Address ");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }


    class FaxValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^(((((((00|\+)49[ \-/]?)|0)[1-9][0-9]{1,4})[ \-/]?)|((((00|\+)49\()|\(0)[1-9][0-9]{1,4}\)[ \-/]?))[0-9]{1,7}([ \-/]?[0-9]{1,5})?)$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid Fax");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }

    class PincodeValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"([0-9]{6}|[0-9]{3}\s[0-9]{3})"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid Pincode");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }

    class NumberValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                if (Regex.IsMatch((String)value, @"^([0-9]*|\d*\.\d{1}?\d*)$"))
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Invalid Number");
                }
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Invalid characters " + e.Message);
            }

        }
    }
}
